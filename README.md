# Mentorship Compact & Lab Manual

John Hoffman, Ph.D. | Assistant Adjunct Professor | Department of Radiological Sciences | UCLA, Los Angeles, CA

## Attribution

This is a modified version of Dr. Ami Bhatt's lab guidelines documents, which itself was "inspired by John Boothroyd's lab expectations document."

## Welcome and some opening thoughts

Welcome and I'm exciting to start working with you!

This lab manual/compact is designed to both give you a clear overview of expectations, day-to-day operations, as well as do some "tone-setting" for the work environment I am striving to build here at UCLA.  If you have any questions or concerns, please reach out to me anytime.

This document is a *living* document, which is part of the reason it will be maintained in Git (more on Git later).  Although it is intended as a starting point, it should always grow and change to reflect the needs of the group as a whole.  As part of joining the lab, I will ask everyone to read and "agree to" the contents of the document.  This is not a contract, but it does imply that certain expectations have been set for both me and the lab members and provides a shared basis on which we can improve lab culture, resolve potential issues, and build a welcoming environment for everyone.  This is very important to me and I take this very seriously.

My name is John Hoffman and I am a CT (computed tomography) physicist here in the department of Radiological Sciences at UCLA.  I am responsible for helping you all to maintain a lab environment that encourages personal and professional growth; I am also fully committed to ensuring that the lab is a happy and fun place to be. I am especially conscious of the interpersonal dynamics of the lab and I place a high priority on keeping our work environment friendly, honest, open, and safe. Maintaining these principles is every lab member’s responsibility and I appreciate your help in contributing to the positive environment of the group. Entering a lab can be intimidating, and I know that you likely have high expectations of yourself and what you will accomplish while you are here. Keep in mind that we are all working towards a common goal and can benefit from one another’s expertise and insights; the lab culture reflects this collective spirit. We are all responsible for ensuring that being a graduate of the "Hoffman lab" communicates a sense of scientific excellence, personal integrity and generosity.

These priniciples apply equally to all in the lab, including myself.

## Our Research

My work to date has been focused on quantitative imaging in CT, as well as the computational realities associated with quantitative imaging in CT.  Quantitative imaging is strongly impacted by all of the processes used to both acquire the data (acquisition) and form the image (reconstruction) and it is my belief that many of the approaches currently being pursued to not adequately account for these factors prior to measuring quantitative image features.  The degree to which this impacts clinical, day-to-day practice is unclear, however I believe these questions will likely need to be answered or at least significantly clarified before we will see widespread translation of quantitative imaging into medical practice.

Although it may seem somewhat orthogonal to the above, I also care deeply about refining our computational tools to make this work more accessible to everyone, including MDs, other PhDs, your fellow graduate students and hopefully one day the general public, as this is also a major obstacle to the widespread clinical availability of quantitative imaging tools.  To truly deploy these systems, we need not only validated, reliable quantitative imaging tests, but also what I think of as "New HPC" to run, scale and leverage these systems.

## Getting Help

Anytime in life can be stressful, but university life and graduate school can be an order of magnitude different for many, particularly for folks who have mental health or other health needs.  It can also be a new place, with new people, and it can be overwhelming.

Please know that I am always available to talk about issues you feel comfortable sharing with me and I will do my best to guide you to needed help, resources, or other guidance.  Although I need to ensure that the lab functions well, part of the lab is you.  Although I can't/won't guarantee that everything you do during our tenure together will be stress-free, your health and wellness are the highest priority for me.  The resources below are from students and other UCLA folks:

Resources:
- TODO

## Communication

Although we're striving for a fun and enjoyable place to work, this is still first and foremost a job.  All communication between lab members is expected to be professional, courteous, and considerate.  That doesn't mean everything on slack has to be 100% work related, but just keep in mind that as we shift to more text-based communication, please also keep in mind written tone and be aware that it may not always be read the same as it was written, and not everyone will share your sense of humor.  What might be funny to you, may be deeply hurtful to someone else.  A good general guideline is that if you wouldn't say it in a Thursday CVIB research meeting, it shouldn't be said on any of the below messaging systems.

### Slack

Slack is a modern instant messaging system ripped from the old IRC systems of yore and is currently the preferred messaging system for the lab.

We are part of the UCTech slack system, which your mednet email address should allow you to join.  Please ping one of the other lab members or myself over email if you're having trouble getting started.

Slack has "channels" that are essentially chat rooms between groups of people.  They can be public or private (although once they're private, they cannot be made public again, so keep that in mind!).  Some common ones in CVIB are #cvib, #simplemind, and #freect.

You can also direct message (DM) individuals.  This is generally my preferred messaging strategy, rather than channels.

Slack is not secured for PHI or any sort of protected communications.  Do NOT share any such information over slack.

### Email

For longer messages (such as project results, manuscript drafts, etc.) email is preferred.  For all lab-related email, you are expected to utilize your UCLA-provided Mednet email account.  This ensures compliance with UCLA policies.  Please do not use any other email address for work-related communication.

### Phone and texting

I will share my cell number with you (not putting it here though since this is intended to be publicly available).

As a member of the lab, you should feel free to contact me via phone or text.  For work-related items, please keep in mind that Slack is preferred, then email, then text/phone.  If something needs my urgent or immediate attention, feel free to ping me via text to check my email or check Slack.

If you needed to contact me for lab, or personal safety issues please *call or text* me immediately and/or call 911 (if appropriate).

### Message retention

Both Mednet email and Slack are managed by the University and/or the UC System and I ultimately have no control over them.  Although they are private for all intents and day-to-day purposes, please do not treat these messaging channels as truly private if ever there were a need for such a communication channel.

### Lab Communication "Hours"

#### Normal messaging

I generally expect folks to be receiving and responding to messages during normal work hours: 9am(ish)-6pm(ish) weekdays.  I usually check email and Slack first thing in the morning, but will not respond to most messages until later in the day at some point, usually after 4pm.  Shorter messages I'll usually get to earlier.

#### After hours messaging
In general, we have been comfortable with folks messaging outside of normal work hours, however, for anything sent after normal work hours (including weekends) **there is no expectation of a prompt response**.  I am often the worst offender since after hours is sometimes the only time I get to actually work on the research.  If this does not work for you (or you discover that this does not work for you), please let me know ASAP.

### Communicating with John

It is my goal to be available to members of the lab as much as possible.  I also have to balance this with my own set of responsibilities and work time.  That does unfortunately lead to me being difficult to access at times.

Here are some random thoughts about the best way to contact me:

- For most things, please message me on Slack first (including stuff like setting up a time later in the day to talk)
- I get a lot of email and honestly, I'm just not that great at it (nor do I particularly care to be)
- Shorter emails are likely to get attention and response much quicker than long multi-paragraph emails with 2-3 attachments (I often get 3-4 of those per day and cannot/will not read all of them)
- Long emails are fine, however... (see next bullet point)
- For all emails, please be **direct and brief about what you need from me** in your email (as much as possible), preferably in the first few sentences
- Please don't be afraid to use the "high importance" setting if it's something you really need me to look at
- Please also ping me on Slack to let me know to be on the lookout for such an email

### Calendars

I use the UCLA-provided Outlook calendar as my work calendar.  Being a part of Mednet, you also have access to an Outlook calendar and I will use outlook to share meeting invites, manage my schedule, etc.

### "But I prefer to use..."

You're free to manage your work life in a way that works for you, however the expectation is that it does not encumber the normal operations of the lab described in this document, nor should it add additional overhead for your colleagues (including me).  If you have a suggestion for a new tool, modification to the workflow, etc. I'm happy to hear about it and we'll see what we can do, however please don't hold your breat.  Keep in mind that the point of being in a lab is to **do good science**, not spend all day reading and responding to messages, so I am reluctant to add or change messaging systems absent a very good reason to do so.

## Lab Software

### Git and GitLab

Perhaps the most important piece of software you will utilize in this lab is Git and GitLab.  Git is version control software primarily intended for code and documentation.  GitLab is a company that provides a centralized platform for Git repository management that also provides features such as continuous integration (for testing) and deployment.

Everyone in the lab is expected to have a gitlab.com account ([sign up here!](https//gitlab.com/users/sign_up)) and this is the account you will use to push/pull the lab code to and from the various repositories we manage and use.

You will have privileged access to most of the lab's code, so I also request that you take two additional steps with your GitLab account:
- Enable two-factor authentication ([instructions](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html))
- Only push/pull code with SSH ([instructions](https://docs.gitlab.com/ee/user/ssh.html))

**Do not take the security of your GitLab account lightly**.  Not all of our code is publicly available and it represents over a decade of development.  I consider my GitLab account to be the among the *most* important accounts I maintain, second only to my primary gmail account. Direct access to those repositories is almost certainly critical for you to do your job, however you should consider it a privilege.  I will revoke or limit access if there are problems with security.

TODO: Provide more resources for getting started with get

### Zotero

We utilize [Zotero](https://zotero.org) for journal artical reference management and sharing in the lab.  You will be added to the CVIB lab license when you join which should provide you with unlimited storage and sharing.  Please let me know if you need additional information about reference management with Zotero.

### Document Preparation

I am not (yet) super picky about how you do document/manuscript prep.  Mostly, you should use a format that is amenable to your coauthors who will also be contributing to the writing, editing, revision, etc.  This may change depending on the author group you are currently working with.

As a fellow enjoyer of Latex, I am willing to *try* [Overleaf](https://www.overleaf.com/). However if it encumbers the writing or manuscript preparation process, we will fall back on Microsoft Word (I know, I know... I hate it too but it does work enough to get the job done and UCLA provides licenses).

## Computers

CVIB provides us with quite a bit of computing "grunt" when we need it.  We (namely Mitch Murphy mimurphy@mednet.ucla.edu) manage our own server room in which we have:

- Lambda (GPU server with.... TODO: provide some specs)
- TODO: the rest of them?

### Desktops, laptops, and more

In addition to the servers, I will make sure that you also have a suitable computer for more day-to-day work around the lab.  This computer will be your responsibility to take care of, and ensure it's safe and secure.

This *can* be a personal computer if you would like, and using your personal computer does streamline certain operations, however my preference is that you are provided with a computer through the lab and secured by DGIT as this provides certain protections for you as well as the lab in the event something were to happen to the computer.

The specifics of this computer will be a part of the onboarding discussion between you and myself when you join the lab, as not everyone will need or want the same thing.

### Getting computer support

TODO
- DGIT email, phone, etc.
- DGIT help desk 
- My favorite resources
- When to come to John

## Human Subjects Research

As a part of the lab, you are entrusted with and will interact with Protected/Patient Health Information (PHI).  This often represents deeply personal and private information about individuals and we must protect in every way that we can.  This trust is a privilege, however it is also critical to the work that we do.  Any violation or lapse in care for the privacy of study subjects (including accidental) is something that will be be taken extremely seriously.

To that end, the following are some good rules to keep things secure, safe, and private:

- **Don't work with PHI if you don't have to** (this is the most important, easiest, and best way to avoid problems)
- **Never** communicate PHI over Slack
- Avoid communicating PHI over email (this should rarely if ever be necessary)
- If communicating over email (TODO: I think it's the "secure:" preamble, but I honestly don't remember)
- Any printed PHI **must** be stored in locked cabinets and should be shredded if no longer needed

In our work currently, your exposure to PHI should only be during the deidentification process, if at all.

To that end:

- Only access PHI while physically in the lab (any possible exception to this should be discussed with me ahead of time, this includes accessing via VPN)
- Only perform deidentification of patient data on DGIT-approved, encrypted devices (i.e. lab computers)
- **Never** store PHI on laptops or other portable devices (you should not need to do this as part of my lab; if you think you might, let's discuss)
- Avoid storing large amounts of deidentified patient data on laptops or other portable devices (this is mainly to protect you)

**Note:** Finding unsecured PHI in any form (paper, electronic, etc.) is grounds for public loss of trust and very hefty fines, sometimes up to $1,000,000. Many institutions have suffered the consequences of these mistakes, including UCLA, which can be as simple as leaving a backpack with PHI on a train, getting your laptop or phone stolen, or discussing PHI in public places or with inappropriate individuals (family, friends, strangers, etc.). Every couple of years it makes national news when a prestigious institution is fined hundreds of thousands of dollars and falls under intense scrutiny for violation of PHI protection.  Don’t let it be us!

## IRB Protocol

For every project using human subjects there must be an approved IRB (Institutional Review Board) protocol along with completion of the appropriate training courses. To participate in research stemming from these human subjects you must be added to an existing IRB protocol or create your own protocol.  I or another staff member will facilitate this process for lab members. 

IRB protocols exist to protect the patients and donors we work with, which is a top priority of our lab and the University. New IRB protocols must be submitted for projects proposing research with a novel donor community (ie. beginning collection from a new donor group, using a collaborator’s donor samples, etc.).

## Lab Matters

### Onboarding

TODO:
- Access to MedNet
- GitLab account
- List of required trainings

### Personal Behavior

The Hoffman lab, CVIB, and UCLA are wholly committed to maintaining a professional and supportive community. Professionalism encompasses respect for colleagues in every department at every rank, visitors, and vendors; it means maintaining the highest standards of integrity, working hard, and being proud of your work; professionals are reliable, honest, and present themselves in a competent and confident manner. Support entails academically challenging and bettering each other via constructive criticism, teaching, and collaboration; being fully inclusive, regardless of gender, race, nationality, etc.; supportive lab members will place importance on the success of their colleagues, lab, and University. Please keep in mind that in addition to representing yourself, you also represent UCLA, CVIB, and myself and my own work (via the lab), and **your behavior within and outside of the University is a reflection on us all.**

Within my lab and CVIB, we have our own certain expectations of individual behavior. We are a large group, space is limited, and working forty hours a week in one room with the same people can be stressful. The goal of having a peaceful and comfortable lab hinges on members acting with maturity and discretion. Respect your colleagues’ space, belongings, lifestyles, and opinions.  If you feel that your are unable to work in the environment we have provided for you, please come talk to me and we will try to identify an alternative.

While stimulating academic debates are encouraged, arguing is not, and serious or repeated offences will be brought to Ami’s attention. That being said, we are all adults and personal and interpersonal issues should be solved by those involved whenever possible.

Humor can be in bad taste. Videos/jokes/sayings/stereotyping etc. that involve potentially hurtful material should not be shared in the workplace. Please be conscientious of who is around you and what impression you may be making.  Furthermore, such humor can easily tip into the realm of creating a "hostile work environment" which is handled not through the lab, but through UCLA.

John's Note: this section is largely copy/paste from the Bhatt lab since it is well-written, to-the-point, and says most of what I want to say.  However, I would also like to go one step further and say that I *personally* do not have patience for humor derived from stereotyping, cruelty or meanness, jokes about protected classes, etc. and will not tolerate this in my lab.  Period.  These jokes, no matter the audience, are cheap and offensive *to me*.  Please let there be no uncertainty that any such incidents will not be taken lightly and repeated infractions could be considered grounds for dismissal.

### Your Research

We want your research projects to be something you are passionate about. At the same time, we expect them to contribute to the larger goals of the lab. Project ideas should always be run by me, and then in a group setting where you can get immediate feedback. Two minds often work better than one, and this feedback may spark new ideas, reveal issues with your original idea, or help you develop details of the best way to move forward. Once a project is underway, feedback should be continually sought out and provided. Your greatest resource is access to and the input from your colleagues.

Research methods and data are expected to be meticulously and faithfully recorded.  Each lab member should keep a notebook or binder. Your research is meant to move our field forward, which is impossible without reproducible and replicable procedures/results. **Fabrication and/or falsification of results and plagiarism are wholly unacceptable** and will not only affect your reputation but those of collaborators, the PI, the department, and the University. Concerns of academic misconduct should be brought to my attention immediately.

### Recording your research

#### Pushing code to GitLab

Much of your work with me is likely to be programming/code-based, which Git does an excellent job of recording.  However, this does not work if you are not regularly pushing your code!!!

**Please aim to push code at least once per day to a remote branch.**  This allows me to more easily assist you if you are stuck, as well as provides a "cheap" way to back up your work should something happen to the repository, or you temporarily/permanently don't have access to the machine you were previously working on.

#### Lab Notebooks

I would like all members participating in research to keep a lab notebook/journal.  I'm intentionally leaving this a little bit unspecified for the time being while I work to identify some software that would allow us to centralize this information.  More broadly though, the purpose of the lab notebook is to provide a resource primarily for *your* reference, however one that will almost certainly be useful to me and your other lab mates.

Requirements: 
- Always date your entries!
- At minimum, 1-2 sentences about the work done each day 
- Indicate the overall purpose of the experiment.
- List materials and methods.
- Include your calculations! **It’s very important to be able to come back and check the accuracy of these.** Even incorrect simple concentration calculations may be a frustrating source of error, more easily solved with reference to your scratch work. (Screenshots or pictures may be easier here if you're not keeping a physical notebook)
- Show your results. Photos, spreadsheets, graphs, tables, etc. Describe the significance.

Good things to take notes on:
- Goal of the current work/day
- Questions that arise during work
- New project ideas (or features for software)
- What's working well and what could be improved

I personally maintain a physical notebook kept similar to a computer log: [current time] - [message].

When you complete your time in the lab, your notebook must remain in the lab (I understand if you get attached to a physical copy; please see my note below). In many cases, I will be responsible for maintaining these notebooks for a period of at least 3 years (however in certain instances up to 7+ years) after completion and publication of any work from the lab. If you would like to keep a copy of your results, you are welcome to do so - but the original must, by law, stay in the lab.

Note from John: It may be silly, but I myself love a good notebook and get attached to my notebooks/writing.  This is a large part of the motivation to keep your notes in a software system that will be considered the primary location for note taking.  Please consider this when thinking about where to take notes.  If this is a signficant issue for you, please come talk with me.

### Lab Events

TODO: I will add this section when it become more relevant!

### Lab Meetings

We hold one weekly lab meeting on Fridays, from 2-3pm PT.

We're adopting elements of Corey Arnold's style of lab meetings:

- Primary meeting is limited to **one hour**
- Everyone should prepare ONE slide discussing 
  - Work done since last meeting
  - Upcoming work for the next week
  - Problems or issues for assistance
- Everyone will present their slide during the initial one hour slot
- If there are additional topics or areas needing "deep dives", I will plan to continue for *up to* one additional hour (additional offline followups can be scheduled)

Everyone is expected to attend this meeting unless otherwise arranged with me.

I very much appreciate (more than you know) that meetings can be a real burden on your time, so if you have alternatives and suggestions to improve lab meetings, please share them with me.  This should be a productive, useful hour of the week where my goal is that everyone walks away with 1-2 action items, and some input from me and lab mates on your progress.  If you feel that this format isn't working for you (or the group as a whole), please bring this to my attention and we'll troubleshoot.

### One-on-one Meetings with John

Each lab member will meet one-on-one with me weekly or bi-weekly (your choice). How this time is spent is largely going to be driven by you, but some good topics are progress, planning and future directions, troubleshooting, etc.

It's important that we settle into a routine, so please identify a time of the day/week with me that we can both consistently block off.  Please reschedule ASAP if you are unable to attend a given session, and I will do the same.

If you need additional support outside of normal times, you can always reach out on Slack or swing by my office (if I am in).  I may not always be able to drop everything, but if I can't, I will set up a time to meet with you ASAP.

## Sharing your work

A huge component of science is presenting your work to your colleagues and maybe even the general public, and for many of you, part of my job is to help you develop this skill.  That includes submitting abstracts to and attending conferences, presenting posters and talks at conferences, and finally writing manuscripts.

### Abstract submissions and conference attendance

TODO: Add more information to this section

- The expectation is that you are submitting 2-3 abstracts per year to conferences
- Our "main" conferences are AAPM, RSNA, and SPIE, but I encourage you to seek out and submit to additional conferences, and in other topic areas
- I will make sure every student can attend at least one conference per year, however in general we will work to cover travel for conferences where you get talks/posters accepted.
- Please understand that we may not be able to cover all travel, however we won't send you anywhere you would have to pay for yourself.

If you are planning to submit to a conference, you should discuss topic ideas with me at least 4 weeks prior to submitting (and clearly tell me that you are intending to submit).  You don't need to have everything planned out and ready to go, but I do need a heads up to plan/accomodate and know what's being submitted.

In general, I'd like everyone to plan on attending at least *one* conference per year, and the above guidelines still apply even if you do not wish to travel for the conferences (i.e. you are still expected to submit abstracts).  If you, like me, sometimes struggle with a lot of travel, please set up a time to discuss an accomodation or alternative with me.

### Papers

Papers and manuscripts are the "official" output of our work and among the most important components of what we do.  Although it's never too early to start thinking about what in your work is publishable, I don't expect you to be submitting for publications until years 3-5 of grad school (may not apply to you if you are not a graduate student!).

### Social media

In general, I'd like to encourage you to share work that you're excited about on social media if you choose to do so, however please understand that this is a shared lab and none of us are working in full isolation.  I must as you to be extremely careful of the following:

1. **Patient privacy - PHI** - absolutely, 100% positively never share any PHI on social media.  This supercedes any lab policies and could result in fines that you or the lab are responsible for.  If there is **any** doubt about something you'd like to post, you really must run it by me **prior to posting.**  This is very much **not** an "ask forgiveness, not permission" sort of issue.  If you are at all unclear on this policy, talk to me immediately.
1. **Patient privacy - Consideration for the patient** - We collect data from UCLA from patients actively undergoing treatment or screening for illness.  This information is private and personal, and although it is relevant to the science that we do we must always present it in a way that is considerate of the patient, even when it is no longer considered PHI (and whether or not it is on social media or in something like a professional presentation).  The best way to think about this is to imagine that it's your parent, your friend, or your significant other's data, and consider how you would like to see that written about or posted.
1. **Patient privacy - Can you tell how serious this is yet?** - In general, I will discourage you from posting any real patient images or data to a personal social media account.  If you have something you'd like to post that does include a patient/subject image, I must insist that you run it by me first and failure to do so limits my ability to help you should it turn out to have been inappropriate.  Again, if there's any uncertainty about this policy, please come discuss with me immediately.
1. **Protecting our work** - It may not always feel like it, but our research is cutting edge, and not all ideas/details can or should be shared at all times (even if it will eventually be made public).  There are a number of reasons for this including:
   - Work may not be entirely yours to share (collaborator, fellow lab mate, company, etc. may have provided something that made this possible)
   - Work may not be validated yet, and sharing unconfirmed results even informally could be perceived as "publishing" false results (just look at the recent high profile Nature retractions)
   - Work could be proprietary (either owned by the lab, UCLA, or a third-party)
   - Breakthroughs can get "scooped" by other research groups or organizations and for better or worse, this can interfere signficantly with funding opporunities

All of the above being said, I want you to be excited to share and develop your communication skills around your research, so I am happy to work with everyone to find clever and exciting ways to do this including using new media. 

If there's interest and motivation to maintain it, we can discuss creating some official lab accounts on various platforms (currently there are none).

#### Authorship

TODO: this is an important section, however likely won't become too relevant with Josh for a little bit longer. I will add details when that time comes.

### Leaving the lab

TODO: I will fill this section out as I have time and/or it becomes relevant.

## Individual Expectations

### Everyone

#### Work hours and remote work

I do not have any enforced work hours for the lab.  I understand not everyone wants to, or is able to keep the same schedule.  That being said, science usually benefits from collaboration and that is signficantly easier if we are all physically in the same place regularly.  Thus, I expect that most people will be "at work" between 10am-5pm daily, and working 8 hours per day.

I also expect everyone to be reachable and responsive during normal work hours (9am-6pm).  That doesn't mean you always have to keep your email or Slack open, but your lab mates and I should be able to ask you questions and get responses with a reasonable time frame (a few hours is usually ok), unless otherwise arranged with me.

Post-COVID, many of us enjoy working at home (WFH) a few days per week.  I am totally fine with this, and the nature of the lab's work being largely computing-based is actually quite conducive to remote work.  That being said, there is something to be said for working together, whiteboarding, discussing, etc. in person that just doesn't translate over zoom.  Because of that fact, I expect the following:

- You should plan to be in the office *at least* 2-3 days per week, preferably on a predictable schedule that aligns well with your project collaborators (including me)
- Lab meetings should be attended in person; please let me know if you cannot attend in person and we can accomodate Zoom

My schedule lately (as of 7/18/2023) has been the following (subject to change):

- Monday: WFH
- Tuesday: office
- Wednesday: WFH/office (50/50 chance)
- Thursday: office
- Friday: office

If you need exceptions or accomodations to any of the above, please discuss with me.

#### Absences

If you need to be absent from the lab (virtually/physically) for any length of time, you must let me know.  When possible, please give me advanced notice, however I understand that not everything can be predicted ahead of time.  This includes sick days, personal days, vacation, etc.  If you're going to be away for more than 2 days, please set your vacation/away responder on Slack and email.

#### Personal days

LA is sort of a different animal when it comes to day-to-day life and taking care of necessities.  I'd encourage everyone to consider taking roughly one day per quarter (as needed) to step away from the lab and take care of things like errands, shopping, etc.  I hope that doing so can make daily life a little less stressful for you (and perhaps S.O.'s, children, etc.)

This includes the occasional "personal day" if you need to go run errands around LA and don't otherwise have time for it (although please limit this sort of day and always check with me first/let me know before you take the day!).

#### Vacation and burnout

It's very important to me that everyone takes time away from the lab *and* work in general (i.e. don't take vacation and then just remote work!).  I *hope* that you grow to enjoy doing the work that we're doing so much that it becomes difficult to put down, but practically speaking (1) that won't be the case all (most?) of the time, and (2) you still need to take breaks, let your mind rest, learn new skills, etc.  The expectation is that, in addition to the University holidays (federal holidays, and winter break), everyone will take 2-3 weeks of additional time off.  Furthermore I'd strongly encourage at least one of those weeks (5 business days) to be contiguous.

The vacation serves many purposes, but one you may not actively be thinking about is to give the lab time to learn what parts of our infrastructure, workflow, analysis you're "holding" that others might not fully understand or know how to run.  This helps us understand how/when to hand off information, what to hand off, where documentation might be lacking, etc.

I also think it's very important to have time and space 100% away from work, which means that when you're on vacation I would strongly prefer you are *not* checking or responding to email, Slack, etc.  If an "emergency" arises (in general, this should not happen), we will make sure there is a mechanism to contact you.

If you need more time off than 2-3 weeks per year due to extenuating circumstances, please discuss with me.  Every situation will be different, but please be assured that we will find a way to support you while you look after your health and the health of your loved ones.  Options here are remote work, leaves-of-absence, etc. which we can discuss should the need arise.  In the case of an actual emergency, your health (both physical and mental) and safety should *always* take priority,  please just let me know as soon as possible.

#### Dress code

No official dress code, but please look professional and presentable for all lab actitivities, including regular day-to-day office activities.  Please also be considerate of clothing that could make others uncomfortable (imagery, messages, language, etc.)

You must wear your UCLA badge while in-office as well (this is a UCLA policy, not a lab policy).  If you forget it once in a while, you probably don't need to drive home to get it if it's a normal day, but in general you must have your badge with you at all times.

#### Conduct

I hope the lab culture develops in such a way that we have fun and enjoy our time working together, however first and foremost this is a professional environment and you are expected to behave as such (including all of the policies mentioned in this document).  I am personally more comfortable in a work environment with a happy mix of a little bit of irreverence along with the professionalism, however we will default to "professional" without fail should we need to.  If you have any questions or concerns about this, or need clarification on something specific, please let me know ASAP.  **If you ever feel uncomfortable** with the lab conduct, please also come talk to me ASAP.

**If we are planning to be in the clinic** for case collection, phantom scanning, etc. the expectation is that you are dressed professionally and comport yourself professionally while in the clinic; **this is a higher standard than our day-to-day lab expectations**.  In that moment, your represent UCLA medical center in front of the patient population, who may be going through some of the most challenging times in their whole lives.  Although we can have fun and be affable, please be aware of your surroundings, your appearance, and your behavior in that moment and adapt accordingly.  Badges must also be worn at all times in a visible location while in the clinic.

#### Communication

We all must be respectful and responsive to one another to collaborate effectively.  That being said, active communication and research don't often work super well if you're trying to do both at the same time.  It's important for you to find a system that works for you (including closing email and Slack regularly to concentrate on your work), however I expect communication to be responded to within 24 hours.  If you are unable to address the message when you receive it, please at least acknowledge that you've received the message and provide a timeframe for when you can respond.

I will admit up front that I am not a great with staying on top of email for many reasons (please see the [communicating with John](#communicating-with-john) section above for more info), however this is a "do as I say, not as I do" moment/exception/accomodation (which in general I will try to avoid as I don't like double-standards).  I will do my best to respond to everything within 24 hours, however if I fail to do so please feel free to resend or "bump" it in my inbox/Slack to get my attention; I will not be offended or annoyed!

### Graduate Students

- Work towards your dissertation.  I want everyone to feel fulfilled while here included exploring classes and opportunities that interest you (electives, mentoring, internships, etc.), however your research is what will earn you your degree.

- In general, most students should plan to take 5-6 years to complete their degree (preferably closer to 5, more for your sake than anything!)  If you are strongly motivated to complete your degree in more or less time than this, I am happy to discuss what this might look like with you, but please recognize that most students should expect to stay on the "standard" timeline for many reasons.

- Mentor and guide others in the lab (including undergrads, rotating students, and incoming graduate students).  As you progress, you will become the leading expert in what you do and part of your role in science is communicating this to the generation of scientists that will follow behind you.  Note, this is a **critical** part of your role in the lab and a part of what you are paid to do, so it should never be perceived as "additional" or "extra" work.  If you are concerned that mentoring or guiding someone is consuming too much of your time in the lab, please discuss with me ASAP and we will work to develop a plan.  A great way to cut down on the direct mentoring is to write good documentation for the work you do and the tools you build. :) 

### PI

As the PI of the lab and your chosen mentor, I (John) will:

- Provide mentorship to everyone who chooses to be a part of the lab.  If I cannot mentor you on something specific, I will work with you to find someone who can provide input and guidance on that specific (work-related) topic.
- Respect your time.  As  mentioned above, I appreciate some additional patience with replying to email and slack messages, however if you feel like I've forgotten or missed something and it's been beyond the 24 hour mark (or is urgent), please feel free to remind me via Slack or email.
- Provide timely, constructive, and honest feedback on your ideas, methods, papers, etc. 
- Meet regularly with individuals and the lab.
- Give you credit for your work 
- Respect your goals and what you hope to get out of the program.  I will do my best to provide you with the scientific and professional guidance to help you achieve those goals.
- Be a lifelong mentor, colleague, and friend to you in whatever capacity you choose.  I hope to keep in touch and hear about all of your future endeavours.  I hope that I can also be a source of guidance should you ever need it. 
- Ensure the reputation of this lab remains positive and that our work positively impacts the world around us
- Ensure that our work has the funding it needs to continue, and teach you (as much as you would like) about how the funding in academia works.  This includes pursuing grant funding, but also how to be a good steward of the funds that we are awarded to continue our research.
- Abide by the policies laid out in this document, as all lab members are expected to.  Other than a little bit of patience with email, I am to be held to the exact same standards as all lab members.  Although there is a hierarchy here, the culture, conduct, and professionalism expectation apply equally to everyone, including me.

## Some final thoughts from John

I know the term "safe space" is somewhat overused in our modern culture, however I use it very intentionally here.  I want the lab to be a comfortable, safe space for everyone to grow as scientists.  I want this lab, this group of individuals, this community (a word you will also hear me overuse, unapologetically), to be a source of stability, support, and positivity for everyone here.  Furthermore, I hope that eventually other lab groups to look at our community as a source of inspiration, tone-setting, and healthy lab culture.  I recognize that with many people and personalities, that can sometimes be challenging, however it will remain a key lab priority that will supercede other lab activities if needed.

As I've mentioned elsewhere in this document, I will hold myself to the standards set out in this document the same as any lab member is expected to.  If you ever feel that I am not living up to our community's expectations, I hope that you will pull me aside and discuss with me.  If you are uncomfortable talking to me directly, graduate programs at UCLA (I'll mention PBM specifically here) often provide a mechanism for faculty/student mediation, which I am happy to participate in.  My hope is that issues get identified and corrected before it rises to that level, although I recognize that is not always possible.

Finally, graduate school is challenging on many many levels, and although we haven't often discussed it very openly, it's not uncommon to go through some pretty tough, potentially scary, times while your here.  In those moments, I want you to know that you can use me as a resource if you are comfortable doing so.  I, as well as many of your lab mates, have been through many of those same experiences in ways that others might find hard to relate to, and those experiences should not be viewed as shameful, or "you're just not working hard enough" or whatever else.  They are real and they are valid.  I don't have an infinite capacity to provide you with support, however as your mentor I intend to mentor not just the researcher in you, but the whole person.  I am very serious when I say that your health and safety and the health and safety of your loved ones should come first.

Although you should always feel free to talk about anything with me, please keep in mind a few points:

1. Professional relationships - Although I hope that we will become good friends, while you work for the lab you are first and foremost an employee, and I am your boss.  Not everything is appropriate to discuss with your boss, either in casual conversation or in a "support" capacity.  I ask that we all remain conscious of this, and I will redirect you if needed to alternative resources, or stop the conversation.  This also applies between you and your coworkers.  Know your audience.
2. I am a required reporter - I am mandated by the state of California to report/escalate certain things (namely Title IX violations) to offices within UCLA.  Although I am still more than happy to provide you with support in that moment, please remain aware of this.  Should you need more information on this topic, please set up a time with me to discuss.

## This Document

### Proposing changes and updates

This document is intended to be edited, updated, etc. to reflect the current needs of the lab and its members.  So I encourage you to propose changes to adapt the document(s) as needed while you are a part of the lab.

To do so, please use the usual Git workflow: make your changes on a new branch (e.g. `git checkout -b jhoffman/new-meeting-schedule`), open a merge request for review and discussion, and I (John) will ultimately merge the changes if the lab agrees to them, or if a policy change is needed.

I hope that everyone understands that I do reserve the right to be the final decision maker about what is included in the document, however it is very important to me that everyone can provide input.

### Acknowledging this document

This is a living document and a "compact" which is NOT a contract, however I do feel it's important that everyone acknowledges that they have read, agrees to abide by the contents, and has had an opportunity to contribute changes.

To acknowledge, we use Git.  Below is an example for my acknowledgement.  Please adapt for yourself.

```
git clone git@gitlab.com:jhoffman-ucla/mentorship-compact.git
cd mentorship-compact
git checkout -b jhoffman/ack
cp acknowledgements/empty.md acknowledgements/jhoffman.md
git add acknowledgements/jhoffman.md
git commit -m "johns compact acknowledgement"
git push -u origin jhoffman/ack
# click link and follow instructions to open a Merge Request
```

## Contact information

John Hoffman
924 Westwood Blvd. Suite 650
Los Angeles, CA 90024

Cell: (919) 928-2525

Please only use my cell number (not the UCLA directory office number).
I screen calls for numbers I don't recognize, so please also always leave a voicemail if I don't answer.

Email: jmhoffman@mednet.ucla.edu
Personal Email: johnmarianhoffman@gmail.com

Please use my work email, however please keep my personal email in your contacts in the event that I lose access to my MedNet email.
